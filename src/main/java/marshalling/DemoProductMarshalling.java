package marshalling;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import model.ERPData;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import model.OPCDataItem;


/**
 * Demo class marshalling XML to Java Object and vice versa
 * @author julian
 *
 */
public class DemoProductMarshalling {

	private Logger _log = LogManager.getLogger(DemoProductMarshalling.class); 
	
	private JAXBContext ctx; 
	
	private Marshaller _marshaller ; 
	
	private Unmarshaller _unmarshaller; 
	
	
	public DemoProductMarshalling() {
		try {
			ctx = JAXBContext.newInstance(OPCDataItem.class);
			_marshaller = ctx.createMarshaller();
			_marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			_unmarshaller = ctx.createUnmarshaller();
		} catch (JAXBException e) {
			e.printStackTrace();
		} 
	}
	
	public void run() {
		_log.debug("Creating a java object");
		//Create a java object 
		OPCDataItem<Integer> opcItem = new OPCDataItem(); 
		opcItem.setItemName("Aleks");
		opcItem.setStatus("GOOD");
                opcItem.setOverallStatus("OK");
		opcItem.setValue(100);
		opcItem.setTimestamp(new Date().getTime() );
		
		
		//generate XML
		StringWriter sWriter = new StringWriter(); 
		String xmlString = null; 
		try {
			_log.debug("marshalling java object to XML");
			_marshaller.marshal(opcItem, sWriter);
			sWriter.flush();
			xmlString = sWriter.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		_log.debug(xmlString);
		
		_log.debug("Creating a java object from XML string");
		
		StringReader sReader = new StringReader(xmlString); 
		try {
			OPCDataItem opcItem2 = (OPCDataItem) _unmarshaller.unmarshal(sReader);
			_log.debug("Object created: " + opcItem2.toString());
			
		
		} catch (JAXBException e) {
			e.printStackTrace();
		} 
		
		
		
		_log.debug("Marshalling object to JSON:");
		Gson gson = new Gson();
		
		String jsonString = gson.toJson(opcItem);
		_log.debug(jsonString);
		
		_log.debug("Unmarshalling from JSON ...");
		
		DemoProductMarshalling tmpData = gson.fromJson(jsonString, DemoProductMarshalling.class);
		_log.debug("Unmarshalling from JSON done: " + tmpData.toString());
		
	}
}
