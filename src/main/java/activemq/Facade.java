package activemq;

import gui.FrameOPC;
import javax.swing.SwingUtilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author
 */
public class Facade {

    private static Facade INSTANCE = null;

    private FrameOPC frame = null;

    private Facade() {

    }

    public FrameOPC getFrame() {
        return frame;
    }

    public void setFrame(FrameOPC frame) {
        this.frame = frame;
    }

    public void setPhotocellTime(String txt) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                frame.setPhotocellTime(txt);
            }

        });

    }

    public static synchronized Facade getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Facade();
            System.err.println("Facade::Facade() ");
        }
        return INSTANCE;
    }

}
