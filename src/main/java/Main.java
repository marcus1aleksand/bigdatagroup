

import activemq.Facade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import esper.EsperTest;
import activemq.QueueConnection;
import activemq.QueueConnectionUsingCamel;
import esper.EsperModel;
import gui.FrameOPC;
import marshalling.DemoAleksMarshalling;
import marshalling.DemoMarshalling;
import stateless4j.ProdMachine;
import stateless4j.TestMachine;


/**
 * Just comment out the code pieces you want.
 * @author julian
 *
 */
public class Main {

	Logger _log = 	LogManager.getLogger(Main.class);
	
	public static void main(String[] args) {
		
                FrameOPC frame = new FrameOPC();
                frame.setVisible(true);
                Facade.getInstance().setFrame(frame);
            
                
		//JAXB Marshalling / Unmarshalling 
//		DemoMarshalling demo = new DemoMarshalling(); 
                DemoMarshalling customer = new DemoMarshalling(); 
		customer.run();
                
		DemoAleksMarshalling demo = new DemoAleksMarshalling(); 
		demo.run();
		
		//Message Queue Connection using JMS 
		QueueConnection q = new QueueConnection();
		
		//Message Queue Connection using JMS via Apache Camel 
		QueueConnectionUsingCamel qc = new QueueConnectionUsingCamel(); 
		qc.run();
		
		// State Machine 
		ProdMachine p = new ProdMachine(); 
		p.run(); 
	
		//Esper test 
		//EsperModel esperTest = new EsperModel(); 
		//esperTest.run();
	}
}
